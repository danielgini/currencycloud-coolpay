# README #

### Assumptions ###

* Services and its implementation were providen, there is no UI to interact dinamically with the services but
	it is possible to use the unit tests created plus run the main application with a fixed example.

* There are negative tests, null checks and exceptions handled but it was not done in an exhausted manner but
	to check the main potential issues.

* Some methods might seem they do nothing in particular but I was aiming to separate the application services
	from the coolpayapi services