import application.config.AppConfig;
import enums.Currency;
import model.Payment;
import model.Recipient;
import model.Token;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import services.AppServices;

import java.math.BigDecimal;

import static org.junit.Assert.assertNotNull;

/**
 * @author daniel.gini
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={AppConfig.class})
public class AppServicesTests {

    private final String username = "DanielG";
    private final String apikey = "4D484C5F29E2BEE0";

    @Autowired
    private AppServices appServices;

    @Test
    public void TestAuthentication() {
        try {
            appServices.authenticate(username,apikey);
            assert (true);
        } catch (Exception e) { assert (false); }
    }

    @Test
    public void TestAddRecipient() {
        try {
            Token token = appServices.authenticate(username, apikey);
            Recipient recipient = appServices.addRecipient(token, "Jack Michael");
            assertNotNull(recipient);
        } catch (Exception e) { assert (false); }
    }

    @Test
    public void TestSendMoney() {
        try {
            Token token = appServices.authenticate(username, apikey);
            Payment payment = appServices.sendMoney(token, "523be531-a76f-4df0-9ecf-e5335e8fa0ba", new BigDecimal(10.8), Currency.GBP);
            assertNotNull(payment);
        } catch (Exception e) { assert (false); }
    }

    @Test
    public void TestSuccessfulPayment() {
        try {
            Token token = appServices.authenticate(username, apikey);
            assert(appServices.wasPaymentSuccessful(token, "58f9116e-ecb5-40b9-a37d-999c95656cd5"));
        } catch (Exception e) { assert (false); }
    }

    @Test
    public void TestUnsuccessfulPayment() {
        try {
            Token token = appServices.authenticate(username, apikey);
            assert(!appServices.wasPaymentSuccessful(token, "eada2361-abbf-4880-b8c5-98bd60a815fa"));
        } catch (Exception e) { assert (false); }
    }
}