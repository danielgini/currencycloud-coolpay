import application.config.AppConfig;
import coolpay.services.Authentication;
import coolpay.services.Payments;
import coolpay.services.Recipients;
import exceptions.AuthenticationException;
import exceptions.NoSuchRecipientException;
import model.Payment;
import model.Recipient;
import model.Token;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author daniel.gini
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={AppConfig.class})
public class CoolPayAppServicesTests {

    private final String username = "DanielG";
    private final String apikey = "4D484C5F29E2BEE0";

    @Autowired
    private Authentication authServices;

    @Autowired
    private Recipients recipientServices;

    @Autowired
    private Payments paymentServices;

    @Test
    public void TestLogin() {
        try {
            Token token = authServices.login(username,apikey);
            assertNotNull(token);
        } catch (Exception e) { assert (false); }
    }

    @Test
    public void TestCreateRecipient() {
        try {
            Token token = authServices.login(username, apikey);
            Recipient recipient = recipientServices.createRecipient(token,"AgustinG");
            assertNotNull(recipient);
            System.out.println(recipient.getId());
        } catch (Exception e) { assert (false); }
    }

    @Test
    public void TestListRecipients() {
        try {
            Token token = authServices.login(username, apikey);
            List<Recipient> recipients = recipientServices.listRecipients(token);
            assertNotNull(recipients);
            assertNotEquals(recipients.size(),0);
            recipients.stream().map(rec -> rec.getId() + " - " + rec.getName()).forEach(System.out::println);
        } catch (Exception e) { assert (false); }
    }

    @Test
    public void TestCreatePayment() {
        try {
            Token token = authServices.login(username, apikey);
            Payment payment = paymentServices.createPayment(token,"523be531-a76f-4df0-9ecf-e5335e8fa0ba","11","GBP");
            assertNotNull(payment);
            System.out.println(payment.getStatus());
        } catch (Exception e) { assert (false); }
    }

    @Test
    public void TestCreatePaymentFailed() {
        try {
            Token token = authServices.login(username, apikey);
            paymentServices.createPayment(token,"bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb","11","GBP");
            assert(false);
        } catch (AuthenticationException e) {
            assert (false);
        } catch (NoSuchRecipientException e) {
            assert (true);
        }
    }

    @Test
    public void TestListPayments() {
        try {
            Token token = authServices.login(username, apikey);
            List<Payment> payments = paymentServices.listPayments(token);
            assertNotNull(payments);
            assertNotEquals(payments.size(),0);
            payments.stream().map(pay -> pay.getId() + " / " + pay.getRecipientId() + " - " + pay.getStatus()).forEach(System.out::println);
        } catch (Exception e) { assert (false); }
    }

}
