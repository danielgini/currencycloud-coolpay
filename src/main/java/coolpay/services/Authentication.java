package coolpay.services;

import model.Token;

import exceptions.AuthenticationException;

/**
 * @author Daniel
 */
public interface Authentication extends CoolpayService {

    Token login(String username, String apikey) throws AuthenticationException;
}
