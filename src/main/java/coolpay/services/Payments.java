package coolpay.services;

import exceptions.NoSuchRecipientException;
import model.Payment;
import model.Token;

import java.util.List;

/**
 * @author Daniel
 */
public interface Payments extends CoolpayService {

    Payment createPayment(Token token, String recipientId, String amount, String currency) throws NoSuchRecipientException;

    List<Payment> listPayments(Token token);
}
