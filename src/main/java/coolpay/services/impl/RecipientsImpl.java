package coolpay.services.impl;

import coolpay.services.Recipients;
import model.Recipient;
import model.Token;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

/**
 * @author daniel.gini
 */
public class RecipientsImpl implements Recipients {

    private static final String URL = "https://coolpay.herokuapp.com/api/recipients";

    @Override
    public Recipient createRecipient(Token token, String recipient)   {
        Entity payload = Entity.json("{  \"recipient\": {    \"name\": \""+recipient+"\"  }}");

        Response response = getResponse(URL,payload,"Bearer " +token);
        if (response.getStatus()==201) {
            JSONObject jsonObject = (JSONObject) new JSONObject(response.readEntity(String.class)).get("recipient");
            return new Recipient((String) jsonObject.get("name"), (String) jsonObject.get("id"));
        }

        return null;
    }

    @Override
    public List<Recipient> listRecipients(Token token) {
        List<Recipient> recipients = new ArrayList<>();

        Response response = getResponse(URL,"Bearer " +token);
        if (response.getStatus()==200) {
            JSONObject jsonObject = new JSONObject(response.readEntity(String.class));
            JSONArray jsonArray = jsonObject.getJSONArray("recipients");
            for (int i = 0; i < jsonArray.length(); i++) {
                Recipient recipient = new Recipient((String) jsonArray.getJSONObject(i).get("name"),
                        (String) jsonArray.getJSONObject(i).get("id"));
                recipients.add(recipient);
            }
        }

        return recipients;
    }

}
