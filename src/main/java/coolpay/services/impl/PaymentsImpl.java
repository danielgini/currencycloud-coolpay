package coolpay.services.impl;

import coolpay.services.Payments;
import enums.Currency;
import enums.PaymentStatus;
import exceptions.NoSuchRecipientException;
import model.Payment;
import model.Token;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author daniel.gini
 */
public class PaymentsImpl implements Payments {

    private static final String URL = "https://coolpay.herokuapp.com/api/payments";

    @Override
    public Payment createPayment(Token token, String recipientId, String amount, String currency) throws NoSuchRecipientException {
        Entity payload = Entity.json("{  \"payment\": {    \"amount\": "+amount+", " +
                                                        "  \"currency\": \""+currency+"\", " +
                                                        "  \"recipient_id\": \""+recipientId+"\"  }}");

        Response response = getResponse(URL,payload,"Bearer " +token);
        if (response.getStatus()==422)  throw new NoSuchRecipientException();
        else
            if (response.getStatus()==201)
                return createPayment((JSONObject) new JSONObject(response.readEntity(String.class)).get("payment"));

        return null;
    }

    @Override
    public List<Payment> listPayments(Token token) {
        List<Payment> payments = new ArrayList<>();

        Response response = getResponse(URL,"Bearer " +token);
        if (response.getStatus() == 200) {
            JSONObject jsonObject = new JSONObject(response.readEntity(String.class));
            JSONArray jsonArray = jsonObject.getJSONArray("payments");
            for (int i = 0; i < jsonArray.length(); i++)
                payments.add(createPayment(jsonArray.getJSONObject(i)));
        }

        return payments;
    }

    private Payment createPayment(JSONObject jsonObject)    {
        return new Payment(
                (String) jsonObject.get("id"),
                new BigDecimal((String) jsonObject.get("amount")),
                Currency.valueOf((String) jsonObject.get("currency")),
                (String) jsonObject.get("recipient_id"),
                PaymentStatus.valueOf(((String) jsonObject.get("status")).toUpperCase()));
    }

}
