package coolpay.services.impl;

import coolpay.services.Authentication;
import model.Token;
import org.json.JSONObject;

import exceptions.AuthenticationException;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

/**
 * @author daniel.gini
 */
public class AuthenticationImpl implements Authentication {

    private static final String URL = "https://coolpay.herokuapp.com/api/login";

    @Override
    public Token login(String username, String apikey) throws AuthenticationException {
        Entity payload = Entity.json("{  \"username\": \""+username+"\",  \"apikey\": \""+apikey+"\"}");
        Response response = getResponse(URL, payload);

        if (response.getStatus()!=200)  throw new AuthenticationException();

        JSONObject jsonObject = new JSONObject(response.readEntity(String.class));
        return new Token((String) jsonObject.get("token"));
    }

}
