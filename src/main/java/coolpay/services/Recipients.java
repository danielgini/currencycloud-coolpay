package coolpay.services;

import model.Recipient;
import model.Token;

import java.util.List;

/**
 * @author Daniel
 */
public interface Recipients extends CoolpayService {

    Recipient createRecipient(Token token, String recipient);

    List<Recipient> listRecipients(Token token);
}
