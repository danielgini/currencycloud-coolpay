package coolpay.services;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public interface CoolpayService {

    default Response getResponse(String url, Entity payload)    {
        return getResponse(url,payload,null);
    }

    default Response getResponse(String url, String authorization)    {
        return getResponse(url,null,authorization);
    }

    default Response getResponse(String url, Entity payload, String authorization)    {
        if (payload!=null) {
            if (authorization==null)
                return ClientBuilder.newClient().target(url)
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .post(payload);
            else
                return ClientBuilder.newClient().target(url)
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .header("Authorization", authorization)
                        .post(payload);
        } else
            return ClientBuilder.newClient().target(url)
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .header("Authorization", authorization)
                    .get();
    }

}
