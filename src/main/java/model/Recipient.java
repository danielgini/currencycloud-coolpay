package model;

/**
 * Created by daniel.gini on 01/08/2017.
 */
public class Recipient {

    private String name;
    private String id;

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public Recipient(String name, String id) {

        this.name = name;
        this.id = id;
    }
}
