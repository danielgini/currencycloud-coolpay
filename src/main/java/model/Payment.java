package model;

import enums.Currency;
import enums.PaymentStatus;

import java.math.BigDecimal;

/**
 * Created by daniel.gini on 01/08/2017.
 */
public class Payment {

    private String id;
    private BigDecimal amount;
    private Currency currency;
    private String recipientId;
    private PaymentStatus status;

    public Payment(String id, BigDecimal amount, Currency currency, String recipientId, PaymentStatus status) {
        this.id = id;
        this.amount = amount;
        this.currency = currency;
        this.recipientId = recipientId;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public String getRecipientId() {
        return recipientId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public PaymentStatus getStatus() {
        return status;
    }
}
