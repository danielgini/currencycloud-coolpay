package model;

/**
 * @author Daniel
 */
public class Token {

    private final String token;

    public Token(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return token;
    }
}
