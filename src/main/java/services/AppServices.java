package services;

import enums.Currency;
import exceptions.AuthenticationException;
import exceptions.NoSuchRecipientException;
import model.Payment;
import model.Recipient;
import model.Token;

import java.math.BigDecimal;

/**
 * @author Daniel
 */
public interface AppServices {

    Token authenticate(String name, String key) throws AuthenticationException;

    Recipient addRecipient(Token token, String name);

    Payment sendMoney(Token token, String recipientId, BigDecimal amount, Currency currency) throws NoSuchRecipientException;

    boolean wasPaymentSuccessful(Token token, String paymentId);

}
