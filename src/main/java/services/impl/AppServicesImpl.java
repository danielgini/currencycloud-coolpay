package services.impl;

import coolpay.services.Authentication;
import coolpay.services.Payments;
import coolpay.services.Recipients;
import enums.Currency;
import enums.PaymentStatus;
import exceptions.AuthenticationException;
import exceptions.NoSuchRecipientException;
import model.Payment;
import model.Recipient;
import model.Token;
import org.springframework.beans.factory.annotation.Autowired;
import services.AppServices;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public class AppServicesImpl implements AppServices {

    @Autowired
    private Authentication authServices;

    @Autowired
    private Recipients recipientServices;

    @Autowired
    private Payments paymentServices;

    @Override
    public Token authenticate(String name, String key) throws AuthenticationException {
        Token token = authServices.login(name,key);
        if (token==null) throw new AuthenticationException();
        return token;
    }

    @Override
    public Recipient addRecipient(Token token, String name) {
        return recipientServices.createRecipient(token, name);
    }

    @Override
    public Payment sendMoney(Token token, String recipientId, BigDecimal amount, Currency currency) throws NoSuchRecipientException {
        return paymentServices.createPayment(token,recipientId,amount.toString(),currency.name());
    }

    @Override
    public boolean wasPaymentSuccessful(Token token, String paymentId) {
        PaymentStatus status;
        do {
            status = getStatus(token, paymentId);
        } while (status != null && status.equals(PaymentStatus.PROCESSING));
        return (status != null && status.equals(PaymentStatus.PAID));
    }

    private PaymentStatus getStatus(Token token, String paymentId)  {
        List<Payment> payments = paymentServices.listPayments(token);
        Optional<Payment> opPayment = payments.stream().filter(payment -> payment.getId().equals(paymentId)).findFirst();
        return (opPayment.map(Payment::getStatus).orElse(null));
    }
}
