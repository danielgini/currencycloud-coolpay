package application.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import services.AppServices;
import services.impl.AppServicesImpl;

@Configuration
@ComponentScan
@Import(CoolpayServicesConfig.class)
public class AppConfig {

    @Bean
    public AppServices getServices() {

        return new AppServicesImpl();
    }
}
