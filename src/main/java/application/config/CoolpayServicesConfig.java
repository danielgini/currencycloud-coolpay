package application.config;

import coolpay.services.Authentication;
import coolpay.services.Payments;
import coolpay.services.Recipients;
import coolpay.services.impl.AuthenticationImpl;
import coolpay.services.impl.PaymentsImpl;
import coolpay.services.impl.RecipientsImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class CoolpayServicesConfig {

    @Bean
    public Payments getPaymentServices() {

        return new PaymentsImpl();
    }

    @Bean
    public Recipients getRecipientServices() {

        return new RecipientsImpl();
    }

    @Bean
    public Authentication getAuthenticationServices() {

        return new AuthenticationImpl();
    }

}
