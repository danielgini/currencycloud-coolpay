package application;

import enums.Currency;
import model.Payment;
import model.Recipient;
import model.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import exceptions.AuthenticationException;
import services.AppServices;

import java.math.BigDecimal;

/**
 * @author Daniel
 */
@SpringBootApplication
public class Application implements CommandLineRunner {

    /**
     *  Username: <DanielG> (arg0)
     *  API Key: <4D484C5F29E2BEE0> (arg1)
     */

    @Autowired
    private AppServices appServices;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        String username = "DanielG";
        String apikey = "4D484C5F29E2BEE0";

        if (args.length>=2) {
            username = args[0];
            apikey = args[1];
        }

        try {
            Token token = appServices.authenticate(username, apikey);
            System.out.println("Authentication Success - TOKEN: " + token);

            Recipient recipient = appServices.addRecipient(token, "Mr Bean");
            Payment payment = appServices.sendMoney(token,recipient.getId(),new BigDecimal(35.7), Currency.EUR);
            boolean success = appServices.wasPaymentSuccessful(token,payment.getId());

            if (success)
                System.out.println("PAYMENT SUCCESSFUL!");
            else
                System.out.println("PAYMENT FAILED - TRY IT AGAIN!");

        } catch (AuthenticationException e) {
            System.out.println("AUTHENTICATION PROBLEM: USERNAME OR KEY INCORRECT!");
        }
    }

}
